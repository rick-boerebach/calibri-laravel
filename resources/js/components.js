window.Vue = require('vue');

Vue.component('calibri-recorder', require('./components/VideoJsRecord.vue').default);

const app = new Vue({
    el: '#app'
});