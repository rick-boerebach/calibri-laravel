@extends('layouts.backend')

@section('title', 'Behaviors')

@section('content')
<x-section-header heading="Behaviors" breadcrumb="learn" />
<div class="row">
  <div class="col-12 mb-4">
      <div class="hero text-white hero-bg-image" style="background-image: url('{{ asset('stisla/img/unsplash/eberhard-grossgasteiger-1207565-unsplash.jpg') }}');">
        <div class="hero-inner text-center">
          <h2>Subscribe to behaviors</h2>
          <p class="lead">You have successfully registered with our system. Next, you can subscribe to behaviors with your account.</p>
          <div class="mt-4">
            <a href="#" class="btn btn-outline-white btn-lg btn-icon icon-left">
              <i class="fas fa-plus"></i> Subscribe to behaviors</a>
          </div>
        </div>
      </div>
    </div>
</div>

@foreach($topics as $i => $topic)
  @include('partials.learn.topic', ['topic' => $topic])
@endforeach
@endsection