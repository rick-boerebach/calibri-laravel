@extends('layouts.backend')

@section('title', 'Analytics')

@section('content')
<div class="section-header">
	<h1>Analytics</h1>
</div>
<div class="row">
	<div class="col-sm-9">&nbsp;</div>
	<div class="col-sm-3">
		<input data-type="daterange" class="form-control pull-right"></input>
	</div>
	@include('partials.analytics.voice-mechanics')
	@include('partials.analytics.vibe')
	@include('partials.analytics.dynamics')
	@include('partials.analytics.content')
</div>
@endsection

@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('plugin')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
@endsection

@section('head-js')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js" integrity="sha256-t9UJPrESBeG2ojKTIcFLPGF7nHi2vEc7f5A2KpH/UBU=" crossorigin="anonymous"></script>
<script src="https://cdn.anychart.com/releases/v8/js/anychart-base.min.js"></script>
<script src="https://cdn.anychart.com/releases/v8/js/anychart-tag-cloud.min.js"></script>
@endsection

@section('js')
<script src="{{ asset('stisla/modules/chart.min.js') }}"></script>
<script src="{{ asset('js/pages/analytics.js') }}"></script>
@endsection