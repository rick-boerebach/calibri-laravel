@extends('layouts.backend')

@section('title', 'Capture')

@section('content')
<x-section-header heading="Capture" breadcrumb="capture" />
<div class="row">
  <div class="col-12 mb-4">
      <div class="hero text-white hero-bg-image" style="background-image: url('{{ asset('stisla/img/unsplash/eberhard-grossgasteiger-1207565-unsplash.jpg') }}');">
        <div class="hero-inner text-center">
          <h2>Congratulations</h2>
          <p class="lead">You have successfully registered with our system. Next, you can log in to the dashboard with your account.</p>
          <div class="mt-4">
            <a href="#" class="btn btn-outline-white btn-lg btn-icon icon-left"><i class="fas fa-sign-in-alt"></i> Login</a>
          </div>
        </div>
      </div>
    </div>
</div>

<h2 class="section-title">Recorder</h2>
<div class="row">
  <calibri-recorder uploadUrl="{{ route('capture.store') }}"></calibri-recorder>
</div>
@endsection