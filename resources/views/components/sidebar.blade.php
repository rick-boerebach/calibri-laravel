<div>
    <!-- Sidebar outter -->
    <div class="main-sidebar sidebar-style-2">
        <!-- sidebar wrapper -->
        <aside id="sidebar-wrapper">
            <!-- sidebar brand -->
            <div class="sidebar-brand">
                <a href="{{ route('welcome') }}">
                    <img class="align-center" src="{{ asset('img/logo_dark.png') }}">
                </a>
            </div>
            <div class="sidebar-brand sidebar-brand-sm">
                <a href="{{ route('welcome') }}">
                    <img class="align-center" src="{{ asset('img/bird_white.png') }}">
                </a>
            </div>
            <!-- sidebar menu -->
            <ul class="sidebar-menu">
                <!-- menu header -->
                <li class="menu-header">Menu</li>
                <!-- menu item -->
                <li class="{{ Route::is('dashboard') ? 'active' : '' }}">
                    <a href="{{ route('dashboard') }}">
                        <i class="fas fa-fire"></i>
                        <span>Analytics</span>
                    </a>
                </li>
                <li class="{{ Route::is('learn') ? 'active' : '' }}">
                    <a href="{{ route('learn') }}">
                        <i class="fas fa-bolt"></i>
                        <span>Learn</span>
                    </a>
                </li>
                <li class="{{ Route::is('profile') ? 'active' : '' }}">
                    <a href="{{ route('profile') }}">
                        <i class="fas fa-user"></i>
                        <span>Profile</span>
                    </a>
                </li>

                @hasrole('admin')
                 <!-- menu header -->
                <li class="menu-header">Admin</li>

                <!-- menu item -->
                <li class="{{ Route::is('admin.index') ? 'active' : '' }}">
                    <a href="{{ route('admin.index') }}">
                        <i class="fas fa-fire"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <!-- menu item -->
                <li class="{{ Route::is('admin.topics') ? 'active' : '' }}">
                    <a href="{{ route('admin.topics') }}">
                        <i class="fas fa-plus"></i>
                        <span>Topics</span>
                    </a>
                </li>

                <!-- menu item -->
                <li class="{{ Route::is('admin.posts') ? 'active' : '' }}">
                    <a href="{{ route('admin.posts') }}">
                        <i class="fas fa-edit"></i>
                        <span>Posts</span>
                    </a>
                </li>

                <!-- menu item -->
                <li class="{{ Route::is('admin.calls') ? 'active' : '' }}">
                    <a href="{{ route('admin.calls') }}">
                        <i class="fas fa-phone"></i>
                        <span>Calls</span>
                    </a>
                </li>

                <!-- menu item -->
                <li class="{{ Route::is('admin.users') ? 'active' : '' }}">
                    <a href="{{ route('admin.users') }}">
                        <i class="fas fa-users"></i>
                        <span>Users</span>
                    </a>
                </li>
                @endhasrole
            </ul>
        </aside>
    </div>
</div>