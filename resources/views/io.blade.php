@extends('layouts.app')

@section('title', 'IO')

@section('content')
<div class="container mt-5">
    <div class="row">
        <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="login-brand">
                <img src="https://ui-avatars.com/api/?name={{ config('app.name') }}&background=fff&color=6777ef&size=100"
                    alt="logo" width="100" class="shadow-light rounded-circle">
            </div>

            @if (session('status'))
            <div class="alert alert-info alert-dismissible show fade">
                <div class="alert-body">
                    <button class="close" data-dismiss="alert">
                        <span>×</span>
                    </button>
                    {{ session('status') }}
                </div>
            </div>
            @endif

            <div class="card card-primary">
                <div class="card-header">
                    <h4>Input & Output</h4>
                </div>

                <div class="card-body">
                    <p class="lead">Nothing to see here..</p>
                </div>
            </div>
            <div class="mt-3 text-muted text-center">
                Want to go to the homepage? <a href="{{ route('welcome') }}">Click here!</a>
            </div>
            <div class="simple-footer">
                Copyright © Calibri 2021
            </div>
        </div>
    </div>
</div>
@endsection