@extends('layouts.backend')

@section('title', 'Sandbox')

@section('content')
  @if(false)
    @include('partials.sandbox.circular-controls')
  @else
    @include('partials.sandbox.capture')
  @endif
@endsection