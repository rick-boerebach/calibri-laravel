<h2 class="section-title">{{ $topic->name }}</h2>
<div class="row">
	@foreach($topic->posts as $j => $post)
		@include('partials.learn.topics.post', ['post' => $post])
	@endforeach
</div>