<div class="col-12 col-sm-6 col-md-6 col-lg-3">
	<article class="article article-style-b" data-id="{{ $post->id }}">
		<div class="article-header">
			<div class="article-image" data-background="{{ $post->avatar_url }}">
			</div>
			@if(isset($post->metadata->badge))
			<div class="article-badge">
				<div class="article-badge-item bg-{{ $post->metadata->badge['type'] }}">
					<i class="fas fa-{{ $post->metadata->badge['icon'] }}"></i> {{ $post->metadata->badge['text'] }}
				</div>
			</div>
			@endif
		</div>
		<div class="article-details">
			<div class="article-title">
				<h2><a href="#" data-action="post.view" data-id="{{ $post->id }}">{{ $post->title }}</a></h2>
			</div>
			<p>{{ $post->content }}</p>
			<div class="article-cta">
				<a href="#" data-action="post.view" data-id="{{ $post->id }}">Read More <i class="fas fa-chevron-right"></i></a>
			</div>
		</div>
	</article>
</div>