<div class="col-12 col-sm-6 col-md-6 col-lg-3">
	<article class="article article-style-b">
		<div class="article-header">
			<div class="article-image" data-background="{{ asset($img) }}">
			</div>
			@if(isset($badge))
			<div class="article-badge">
				<div class="article-badge-item bg-{{ $badge['type'] }}"><i class="fas fa-{{ $badge['icon'] }}"></i> {{ $badge['text'] }}</div>
			</div>
			@endif
		</div>
		<div class="article-details">
			<div class="article-title">
				<h2><a href="#" data-action="post.view" data-id="{{ $id }}">{{ $title }}</a></h2>
			</div>
			<p>{{ $content }}</p>
			<div class="article-cta">
				<a href="#" data-action="post.view" data-id="{{ $id }}">Read More <i class="fas fa-chevron-right"></i></a>
			</div>
		</div>
	</article>
</div>