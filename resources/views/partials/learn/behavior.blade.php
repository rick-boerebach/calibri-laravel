<h2 class="section-title">{{ $title }}</h2>
<div class="row">
	@foreach($posts as $j => $post)
		@include('partials.learn.behaviors.post', $post)
	@endforeach
</div>