<div class="mb-2 {{ isset($first) && $first ? 'mt-4' : '' }}">
	<div class="text-small float-right font-weight-not-bold text-muted">{!! $occurences !!}</div>
	<div class="font-weight-not-bold mb-1">{!! $word !!}</div>
	<div class="progress hide" data-height="6">
		<div class="progress-bar" role="progressbar" data-width="80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
	</div>                          
</div>