<div class="col-md-6 col-xs-12 card">
    <div class="card-header">
        <h4>Vibe</h4>
    </div>
    <div class="card-body row text-center">
    	<div class="col-md-6 col-xs-12 p-4">
    		<strong>Your Vibe</strong>
    		<canvas id="chart_vibe_yours" data-chart="vibe-yours"></canvas>
    	</div>
    	<div class="col-md-6 col-xs-12 p-4">
    		<strong>Others' Vibe</strong>
    		<canvas id="chart_vibe_others" data-chart="vibe-others"></canvas>
    	</div>
    	<div class="col-md-12 justify-content-center p-4">
    		<canvas id="chart_vibe_emotion-intensity" data-chart="vibe-emotion-intensity"></canvas>
    	</div>
    </div>
</div>