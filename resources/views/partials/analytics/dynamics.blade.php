<div class="col-md-6 col-xs-12 card">
    <div class="card-header">
        <h4>Dynamics</h4>
    </div>
    <div class="card-body row text-center">
    	<div class="col-md-12 justify-content-center p-4">
    		<canvas id="chart_dynamics_contribution" data-chart="dynamics-contribution"></canvas>
    	</div>
    </div>
</div>