<div class="col-md-6 col-xs-12 card">
    <div class="card-header">
        <h4>Voice Mechanics</h4>
    </div>
    <div class="card-body row text-center">
    	<div class="col-md-6 col-xs-12 p-4">
    		<strong>Voice Seniority</strong>
    		<canvas id="chart_voice-mechanics_voice-seniority" data-chart="voice-seniority"></canvas>
    	</div>
    	<div class="col-md-6 col-xs-12 p-4">
    		<strong>Voice Deliberateness</strong>
    		<canvas id="chart_voice-mechanics_voice-deliberateness" data-chart="voice-deliberateness"></canvas>
    	</div>
    	<div class="col-md-6 col-xs-12 p-4 align-text-bottom">
    		<strong>Filler Words</strong>
    		<canvas id="chart_voice-mechanics_filler-words" data-chart="filler-words"></canvas>
    	</div>
        <div class="col-md-6 col-xs-12 p-4">
        	<strong>List of your filler words</strong>
        	<div class="col-md-12 justify-content-center p-1" id="n-tag-cloud-container">

    		</div>
        </div>
    </div>
</div>