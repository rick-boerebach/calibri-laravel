@extends('layouts.backend')

@section('title', 'Administration')

@section('content')
<x-section-header heading="Administration" breadcrumb="admin" />
<div class="row">
  <div class="col-12 mb-4">
      <div class="hero text-white hero-bg-image" style="background-image: url('{{ asset('stisla/img/unsplash/eberhard-grossgasteiger-1207565-unsplash.jpg') }}');">
        <div class="hero-inner text-center">
          <h2>Hello admin!</h2>
          <p class="lead">You have the admin role and you can do powerful things with your account.</p>
          <div class="mt-4">
            <a href="#" class="btn btn-outline-white btn-lg btn-icon icon-left">
              <i class="fas fa-fire"></i> I understand!</a>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection