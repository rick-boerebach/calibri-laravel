@extends('layouts.backend')

@section('title', 'Manage Calls')

@section('content')
<x-section-header heading="All Calls" breadcrumb="admin/calls" />
<div class="row">
  <div class="col-md-12">
     @include('partials.admin.calls.table')
  </div>
</div>
@endsection