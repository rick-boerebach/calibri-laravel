@extends('layouts.backend')

@section('title', 'Manage Posts')

@section('content')
<x-section-header heading="Create Post" breadcrumb="Create Post" />
<div class="row">
  <div class="col-md-12 justify-content-center">
     @include('partials.admin.posts.create')
  </div>
</div>
@endsection