@extends('layouts.backend')

@section('title', 'Manage Behaviors')

@section('content')
<x-section-header heading="Create Topic" breadcrumb="Create Topic" />
<div class="row">
  <div class="col-md-12 justify-content-center">
     @include('partials.admin.topics.create')
  </div>
</div>
@endsection