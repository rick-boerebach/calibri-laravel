@extends('layouts.backend')

@section('title', 'Manage Users')

@section('content')
<x-section-header heading="All Users" breadcrumb="admin/users" />
<div class="row">
  <div class="col-md-12">
  	<div class="card">
  		<div class="card-header">
  			<h2>Users Table</h2>
  		</div>

  		<div class="card-body">
  			@include('partials.admin.users.table')
  		</div>

  	</div>
  </div>
</div>
@endsection