@extends('layouts.backend')

@section('title', 'Manage Behaviors')

@section('content')
<x-section-header heading="All Behaviors" breadcrumb="Manage Topics" />
<div class="row">
  <div class="col-md-12 justify-content-center">
    <a href="{{ route('admin.topics.create') }}" class="btn btn-md btn-success">
      <i class="fas fa-plus-circle"></i> Create Topic
    </a>
  </div>
  <div class="col-md-12">
     @include('partials.admin.topics.table')
  </div>
</div>
@endsection