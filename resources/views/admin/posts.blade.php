@extends('layouts.backend')

@section('title', 'Manage Posts')

@section('content')
<x-section-header heading="All Posts" breadcrumb="Manage Posts" />
<div class="row">
  <div class="col-md-12 justify-content-center">
    <a href="{{ route('admin.posts.create') }}" class="btn btn-md btn-success">
      <i class="fas fa-plus-circle"></i> Create Post
    </a>
  </div>
  <div class="col-md-12">
     @include('partials.admin.posts.table')
  </div>
</div>
@endsection