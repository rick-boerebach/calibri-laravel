function reloadUserData() {
	return axios.get('/ajax/user');
}

function fetchChartData(start, end) {
	return axios.get('/ajax/charts', { params: {'start': start, 'end': end}});
}

jQuery(function($) {
	console.log('scripts/api.js is loaded...');

	reloadUserData()
		.then(userData => {
			console.group('User Data');
			console.log(userData);
			console.groupEnd();

		})
		.catch(err => {
			console.log('Error', err);
		})
});