"use strict";

var ChartRange = {
	start: 	moment().subtract(29, 'days'),
	end: 	moment(),
	label: 'Last 30 Days'
};

const ChartColorScheme = [
	'#B73122',
	'#D9621C',
	'#E0B94D',
	'#70A62F',
	'#CB8A34',
	'#47BDAC'
];

const EmotionNames = ['Angry', 'Happy', 'Frustrated', 'Surprised', 'Fearful', 'Disgusted'];

// ChartJS
if(window.Chart) {
	/*Chart.defaults.plugins = {
		tooltip: {
			titleFontSize: 7,
			font: {size: 8}
		}
	};*/

  Chart.defaults.global.defaultFontFamily = "'Nunito', 'Segoe UI', 'Arial'";
  //Chart.defaults.global.defaultFontSize = 9;
  //Chart.defaults.global.defaultFontStyle = 500;
  //Chart.defaults.global.defaultFontColor = "#999";
  Chart.defaults.global.tooltips.backgroundColor = '#000';
  Chart.defaults.global.tooltips.titleFontFamily = "'Nunito', 'Segoe UI', 'Arial'";
  Chart.defaults.global.tooltips.titleFontColor = '#fff';
  //Chart.defaults.global.tooltips.titleFontSize = 11;
  //Chart.defaults.global.tooltips.bodyFontSize = 10;
  Chart.defaults.global.tooltips.xPadding = 10;
  Chart.defaults.global.tooltips.yPadding = 10;
  Chart.defaults.global.tooltips.cornerRadius = 3;
}

function getVoiceSeniorityChartOptions() {
	let options = {
		legend: {
			display: false
		},
		scales: {
			yAxes: [{
				gridLines: {
				  drawBorder: true	,
				  color: '#f2f2f2',
				},
				ticks: {
				  beginAtZero: true,
				  stepSize: 10,
				  steps: 10,
				  max: 100
				}
			}],
			xAxes: [{
				ticks: {
				  display: false
				},
				gridLines: {
				  display: true
				}
			}]
		}
	};

	return options;
}

function getVoiceSeniorityDataset(data) {
	let dataset = {
		borderWidth: 2,
		backgroundColor: '#320221',
		borderColor: '#320221',
		borderWidth: 2.5,
		pointBackgroundColor: '#ffffff',
		pointRadius: 4
	};

	dataset.label = 'Voice Seniority';
	dataset.data = data;

	return dataset;
}


function getVoiceSeniorityChartSettings(chartData) {
	var settings = {};

	settings.type = 'line';
	settings.data = {};
	settings.data.labels = chartData.labels;
	settings.data.datasets = [];
	settings.data.datasets.push(getVoiceSeniorityDataset(chartData.data));

	settings.options = getVoiceSeniorityChartOptions();


	return settings;
}

function getVibeChartOptions() {
	let options = {
		legend: {display: false}/*
		legend: {
			display: false
		},
		scales: {
		yAxes: [{
			gridLines: {
			  drawBorder: false,
			  color: '#f2f2f2',
			},
			ticks: {
			  beginAtZero: true,
			  stepSize: 150
			}
		}],
		xAxes: [{
		ticks: {
		  display: false
		},
		gridLines: {
		  display: false
		}
		}]
		}*/
	};

	return options;
}

function getVibeDataset1() {
	let dataset = {
		/*borderWidth: 2,
		backgroundColor: '#320221',
		borderColor: '#320221',
		borderWidth: 2.5,
		pointBackgroundColor: '#ffffff',
		pointRadius: 4*/
	};

	dataset.label = 'Vibe1';
	dataset.backgroundColor = ChartColorScheme;
	dataset.data = [45, 59, 21, 23, 56, 67];

	return dataset;
}

function getVibeDataset2() {
	let dataset = {
		/*borderWidth: 2,
		backgroundColor: '#320221',
		borderColor: '#320221',
		borderWidth: 2.5,
		pointBackgroundColor: '#ffffff',
		pointRadius: 4*/
	};

	dataset.label = 'Vibe2';
	dataset.data = [35, 69];

	return dataset;
}

function getVibeChartDatasets() {
	return [getVibeDataset1()];//, getVibeDataset2()];
}

function getVibeChartSettings() {
	var settings = {};

	settings.type = 'pie';
	settings.data = {};
	settings.data.labels = ['Foo', 'Bar', 'Fooz', 'Barz', 'Fooo', 'Barr'];
	settings.data.datasets = getVibeChartDatasets();

	settings.options = getVibeChartOptions();


	return settings;
}

function getVibeScatterChartOptions() {
	let options = {};



	return options;
}

function getRandomVibeScatterChartDataset(idx) {
	let dataset = {};

	dataset.label = 'Dataset ' + idx;

	dataset.backgroundColor = ChartColorScheme[idx];
	dataset.data = [];

	for(var i=0; i<6; i++) {
		dataset.data.push({x: Math.floor(Math.random() * 100), y: Math.floor(Math.random() * 20)});
	}

	return dataset;
}

function getVibeScatterChartDataset1() {
	let dataset = {};

	dataset.label = 'Dataset 1';

	dataset.data = [{x: 5, y: 14}, {x: 3, y: 17}];

	return dataset;
}

function getVibeScatterChartDataset2() {
	let dataset = {};

	dataset.label = 'Dataset 2';

	dataset.data = [{x: 3, y: 24}, {x: 7, y: 16}];

	return dataset;
}

function getVibeScatterChartDatasets() {
	let datasets = [];
	for (var i=0; i<6; i++) {
		datasets.push(getRandomVibeScatterChartDataset(i));
	}

	return datasets;
	//return [getVibeScatterChartDataset1(), getVibeScatterChartDataset2()];
}

function getVibeScatterChartSettings() {
	var settings = {};

	settings.type = 'scatter';
	settings.data = {};
	//settings.data.labels = ['Foo', 'Bar'];
	settings.data.datasets = getVibeScatterChartDatasets();
	settings.options = getVibeScatterChartOptions();


	return settings;
}

function getDynamicsContributionChartOptions() {
	let options = {
		legend: {display: false}
	};
}

function getDynamicsContributionChartDataset1() {
	let dataset = {
		borderWidth: 2,
		//backgroundColor: '#320221',
		borderColor: '#320221',
		borderWidth: 2.5,
		pointBackgroundColor: '#ffffff',
		pointRadius: 4,
	};

	dataset.label = 'Dataset 1';
	dataset.data = [45, 59, 78, 22, 89];

	return dataset;

}

function getDynamicsContributionChartDataset2() {
	let dataset = {
		borderWidth: 2,
		//backgroundColor: '#620221',
		borderColor: '#620221',
		borderWidth: 2.5,
		pointBackgroundColor: '#ffffff',
		pointRadius: 4,
		borderDash: [10,5]
	};

	dataset.label = 'Dataset 2';
	dataset.data = [35, 61, 72, 64, 91];

	return dataset;

}

function getDynamicsChartSettings() {
	var settings = {};

	settings.type = 'line';
	settings.data = {};
	settings.data.labels = ['Foo', 'Bar', 'Fooobar', 'Barfoo', 'Oofab'];
	settings.data.datasets = [];
	settings.data.datasets.push(getDynamicsContributionChartDataset1());
	settings.data.datasets.push(getDynamicsContributionChartDataset2());

	settings.options = getDynamicsContributionChartOptions();


	return settings;
}

function getContentChartSettings() {
	var settings = {};

	settings.type = 'pie';
	settings.data = {};
	settings.data.labels = ['Foo', 'Bar'];
	settings.data.datasets = [];
	settings.data.datasets.push(getVibeDataset());

	settings.options = getVibeChartOptions();


	return settings;
}

function initializeVoiceMechanicsCharts() {
	let $voiceSeniorityChartContext = $('[data-chart="voice-seniority"]');
	let $voiceSeniorityChart = new Chart($voiceSeniorityChartContext, getVoiceSeniorityChartSettings());

	let $voiceDeliberatenessChartContext = $('[data-chart="voice-deliberateness"]');
	let $voiceDeliberatenessChart = new Chart($voiceDeliberatenessChartContext, getVoiceSeniorityChartSettings());

	let $fillerWordsChartContext = $('[data-chart="filler-words"]');
	let $fillerWordsChart = new Chart($fillerWordsChartContext, getVoiceSeniorityChartSettings());
}

function initializeVibeCharts() {
	let $vibeYoursChartContext = $('[data-chart="vibe-yours"]');
	let $vibeYoursChart = new Chart($vibeYoursChartContext, getVibeChartSettings());

	let $vibeOthersChartContext = $('[data-chart="vibe-others"]');
	let $vibeOthersChart = new Chart($vibeOthersChartContext, getVibeChartSettings());

	let $vibeEmotionIntensityChartContext = $('[data-chart="vibe-emotion-intensity"]');
	let $vibeEmotionIntensityChart = new Chart($vibeEmotionIntensityChartContext, getVibeScatterChartSettings());

}

function initializeDynamicsCharts() {
	let $dynamicsContributionChartContext = $('[data-chart="dynamics-contribution"]');
	let $dynamicsContributionChart = new Chart($dynamicsContributionChartContext, getDynamicsChartSettings());
}

function initializeContentCharts() {
	let tagData = [
		//{'x': 'Foo', value: 1090000000, 'category': 'Foobars'},
		//{'x': 'Bar', value: 544000000, 'category': 'Foobars'},
		{'x': 'Fooz', value: 6},
		{'x': 'Barz', value: 3},
		{'x': 'Foos', value: 5},
		{'x': 'Bars', value: 1}
	];
	let $contentTopicsChartContext = $('[data-chart="content-topics"]');
	//let $contentTopicsChart = new Chart($contentTopicsChartContext, getContentChartSettings());
	let $contentTopicsChart = anychart.tagCloud(tagData);
	$contentTopicsChart.title('Foobars Cloud');
	$contentTopicsChart.angles([0]);
	//$contentTopicsChart.colorRange(true);
	//$contentTopicsChart.colorRange().length('80%');
	$contentTopicsChart.container('tag-cloud-container');
	$contentTopicsChart.draw();
}

function initializeCharts() {
	initializeVoiceMechanicsCharts();
	initializeVibeCharts();
	initializeDynamicsCharts();
	initializeContentCharts();
}


function getLineChartOptions(zeroToHundred) {
	let options = {
		legend: {
			display: false
		},
		scales: {
			yAxes: [{
				gridLines: {
				  drawBorder: true	,
				  color: '#f2f2f2',
				}
			}],
			xAxes: [{
				ticks: {
				  display: false
				},
				gridLines: {
				  display: true
				}
			}]
		}
	};

	if (zeroToHundred) {
		options.scales.yAxes[0].ticks = {};
		options.scales.yAxes[0].ticks.beginAtZero = true;
		options.scales.yAxes[0].ticks.stepSize = 20;
		options.scales.yAxes[0].ticks.steps = 5;
		options.scales.yAxes[0].ticks.max = 100;
	}

	return options;
}

function getLineChartDataset(chartName, data) {
	let dataset = {
		borderWidth: 2,
		backgroundColor: '#320221',
		borderColor: '#320221',
		borderWidth: 2.5,
		pointBackgroundColor: '#ffffff',
		pointRadius: 4
	};

	dataset.label = chartName;//'Voice Seniority';
	dataset.data = data;

	return dataset;
}


function getChartSettings(chartType, chartName, chartData) {
	var settings = {};

	if (chartType == 'line_scaled') {
		settings.type = 'line';
		settings.data = {};
		settings.data.labels = chartData.labels;
		settings.data.datasets = [];
		settings.data.datasets.push(getLineChartDataset(chartName, chartData.data));

		settings.options = getLineChartOptions(true);
	}
	else if (chartType = 'line_dynamic') {
		settings.type = 'line';
		settings.data = {};
		settings.data.labels = chartData.labels;
		settings.data.datasets = [];
		settings.data.datasets.push(getLineChartDataset(chartName, chartData.data));

		settings.options = getLineChartOptions(false);
	}

	return settings;
}

function initializeFillerWordsTagCloud(chartData) {
	$('#n-tag-cloud-container').html('');

	let tagData = chartData.tags;
	//let $contentTopicsChartContext = $('[data-chart="n-filler-words"]');
	//let $contentTopicsChart = new Chart($contentTopicsChartContext, getContentChartSettings());
	let $contentTopicsChart = anychart.tagCloud(tagData);
	//$contentTopicsChart.title('Filler Words');
	$contentTopicsChart.angles([0]);
	//$contentTopicsChart.colorRange(true);
	//$contentTopicsChart.colorRange().length('80%');
	$contentTopicsChart.container('n-tag-cloud-container');
	$contentTopicsChart.draw();
}

function initializeChartsUsingAjax() {
	invalidateChartsUsingAjax();
}

function produceVibeScatterChartDataset(idx, title, values) {
	let dataset = {};

	dataset.label = title;//'Dataset ' + idx;

	dataset.backgroundColor = ChartColorScheme[idx];
	dataset.data = values;

	return dataset;
}

function produceVibeScatterChartSettings(relevantData) {
	let settings = {};

	settings.type = 'scatter';
	settings.data = {};
	settings.data.datasets = [];
	settings.options = {};

	for(var i=0; i<relevantData.datasets.length; i++) {
		settings.data.datasets.push(produceVibeScatterChartDataset(i, EmotionNames[i], relevantData.datasets[i]));
	}

	return settings;
}

function produceVibeDoughnutSettings(title, values) {
	let settings = {};

	settings.type = 'pie';
	settings.data = {};
	settings.data.labels = EmotionNames;//['Angry', 'Happy', 'Frustrated', 'Surprised', 'Fearful', 'Disgusted'];
	settings.data.datasets = [{
		label: title,
		backgroundColor: ChartColorScheme,
		data: values
	}];

	settings.options = {
		legend: {
			display: false
		}
	};

	return settings;
}

function produceDynamicsChartSettings(chartData) {
	let dataset1 = {
		borderWidth: 2,
		//backgroundColor: '#320221',
		borderColor: '#320221',
		borderWidth: 2.5,
		pointBackgroundColor: '#ffffff',
		pointRadius: 4,
	};

	dataset1.label = 'Your Contribution';
	dataset1.data = chartData.data.yours;//[45, 59, 78, 22, 89];

	let dataset2 = {
		borderWidth: 2,
		//backgroundColor: '#620221',
		borderColor: '#620221',
		borderWidth: 2.5,
		pointBackgroundColor: '#ffffff',
		pointRadius: 4,
		borderDash: [10,5]
	};

	dataset2.label = 'Other\'s Contribution';
	dataset2.data = chartData.data.others;//[35, 61, 72, 64, 91];

	let options = {
		legend: {
			display: false
		}
	};
	
	let settings = {};

	settings.type = 'line';
	settings.data = {};
	settings.data.labels = chartData.labels;
	settings.data.datasets = [];
	settings.data.datasets.push(dataset1);
	settings.data.datasets.push(dataset2);

	settings.options = options;

	return settings;
}

function invalidateVibeDoughnuts(chartData) {
	let $vibeYoursChartContext = $('[data-chart="vibe-yours"]');
	let $vibeYoursChart = new Chart($vibeYoursChartContext, produceVibeDoughnutSettings('Yours', chartData.data['vibe-yours'].data));

	let $vibeOthersChartContext = $('[data-chart="vibe-others"]');
	let $vibeOthersChart = new Chart($vibeOthersChartContext, produceVibeDoughnutSettings('Others', chartData.data['vibe-others'].data));

}

function invalidateVibeScatter(chartData) {
	let $vibeEmotionIntensityChartContext = $('[data-chart="vibe-emotion-intensity"]');
	let $vibeEmotionIntensityChart = new Chart($vibeEmotionIntensityChartContext, produceVibeScatterChartSettings(chartData.data['vibe-emotion-intensity'].data));
}

function invalidateDynamicsCharts(chartData) {
	let $dynamicsContributionChartContext = $('[data-chart="dynamics-contribution"]');
	let $dynamicsContributionChart = new Chart($dynamicsContributionChartContext, produceDynamicsChartSettings(chartData.data['dynamics-contribution'].data));
}

function invalidateContentCharts(chartData) {
	$('#tag-cloud-container').html('');
	//let $contentTopicsChartContext = $('[data-chart="content-topics"]');
	let $contentTopicsChart = anychart.tagCloud(chartData.data['content-topics'].data);
	$contentTopicsChart.title('Content');
	$contentTopicsChart.angles([0]);
	//$contentTopicsChart.colorRange(true);
	//$contentTopicsChart.colorRange().length('80%');
	$contentTopicsChart.container('tag-cloud-container');
	$contentTopicsChart.draw();
}

function invalidateChartsUsingAjax() {
	fetchChartData(ChartRange.start, ChartRange.end)
		.then(chartData => {
			let $voiceSeniorityChartContext = $('[data-chart="voice-seniority"]');
			let $voiceSeniorityChart = new Chart($voiceSeniorityChartContext, getChartSettings('line_scaled', 'Voice Seniority', chartData.data['voice-seniority']));

			let $voiceDeliberatenessChartContext = $('[data-chart="voice-deliberateness"]');
			let $voiceDeliberatenessChart = new Chart($voiceDeliberatenessChartContext, getChartSettings('line_scaled', 'Voice Deliberateness', chartData.data['voice-deliberateness']));

			let $fillerWordsChartContext = $('[data-chart="filler-words"]');
			let $fillerWordsChart = new Chart($fillerWordsChartContext, getChartSettings('line_dynamic', 'Filler Words', chartData.data['filler-words']));

			initializeFillerWordsTagCloud(chartData.data['n-filler-words']);

			invalidateVibeDoughnuts(chartData);
			invalidateVibeScatter(chartData);
			invalidateDynamicsCharts(chartData);
			invalidateContentCharts(chartData);


		})
		.catch(err => console.log('Error loading charts', err));
}

function initializeChartControls() {
	let rangeOptions = {
		'Today': [moment(), moment()],
		'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
		'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		'This Month': [moment().startOf('month'), moment().endOf('month')],
		'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
	};

	$('input[data-type="daterange"]').daterangepicker({ranges: rangeOptions, startDate: ChartRange.start, endDate: ChartRange.end}, function (start, end, label) {
		console.log('[new daterange selected, refresh data] ' + start + ' -> ' + end + ' (' + label + ')');
		ChartRange.start 	= start;
		ChartRange.end 		= end;
		ChartRange.label 	= label;

		console.log('new ChartRange', ChartRange);

		invalidateChartsUsingAjax();
	});
	/*$(".daterangepicker-field").daterangepicker({
		forceUpdate: true,
		callback: function(startDate, endDate, period){
			var title = startDate.format('L') + ' – ' + endDate.format('L');
			$(this).val(title)
		}
	});*/
}

jQuery(function($) {
	console.log('Analytics page is loaded...');
	//initializeCharts();
	initializeChartControls();
	initializeChartsUsingAjax();
});