$(document).ready(function () {
	console.log('document ready -> main');

	const beamsClient = new PusherPushNotifications.Client({instanceId: '27c49421-ceb0-484b-a50d-2bdfe308a5ab',});

	beamsClient.start()
		.then(() => beamsClient.setUserId('user.'+calibriUserId))
		.then(() => beamsClient.addDeviceInterest('calibri'))
		.then(() => console.log('Successfully registered and subscribed!'))
		.catch(console.error);

});