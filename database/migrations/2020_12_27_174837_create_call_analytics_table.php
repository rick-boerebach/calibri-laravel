<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCallAnalyticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('call_analytics', function (Blueprint $table) {
            $table->id();
            $table->foreignId('call_id')->nullable()->index();
            $table->json('statistics');
            $table->json('emotions');
            $table->json('filler_words');
            $table->json('keywords');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('call_analytics');
    }
}
