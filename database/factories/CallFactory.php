<?php

namespace Database\Factories;

use App\Models\Call;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CallFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Call::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        // Random datetime within 30 days from today and starting this week
        $startingDate = $faker->dateTimeBetween('this week', '+30 days');

        // Random datetime of max 12 hours *after* `$startingDate`
        $endingDate   = $faker->dateTimeBetween($startingDate, strtotime('+12 hours'));

        return [
            'user_id' => User::all('id')->random(),
            'metadata' => [
                'source' => 'CallSeeder',
                'title' => $this->faker->text(100),
            ],
            'started_at' => $startingDate,
            'ended_at' => $endingDate,
        ];
    }
}
