<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Database\Seeders\UserSeeder;
use Database\Seeders\CallSeeder;
use Database\Seeders\CallAnalyticsSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            // UserSeeder::class,
            // AppSeeder::class,
            // TestSeeder::class,
            UserSeeder::class,
            //CallSeeder::class,
            //CallAnalyticsSeeder::class,
        ]);
    }
}
