<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	App\Models\User::factory()
    		->times(15)
    		->create()
    		->each(function($user) {
			   $calls = App\Models\Call::factory()->times(rand(2,15))->create();
			   $user->calls()->saveMany($calls);
			});
    }
}
