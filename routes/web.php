<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\LoginController;
use App\Http\Controllers\SandboxController;
use App\Http\Controllers\AjaxController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LoginController::class, 'index'])
	->name('welcome');

Route::prefix('sandbox')->group(function () {
	Route::get('/', [SandboxController::class, 'index'])->name('sandbox');
	Route::get('/permissions', [SandboxController::class, 'permissions'])->name('sandbox.permissions');
	Route::get('/permissions/give-me-admin', [SandboxController::class, 'giveMeAdmin'])->name('sandbox.permissions.give-me-admin');
	Route::get('/seed-a-call', [SandboxController::class, 'seedACall'])->name('sandbox.seed.call');
	Route::get('/seed-a-call/randomized', [SandboxController::class, 'seedACallWithRandomness'])->name('sandbox.seed.call.randomized');
});

Route::prefix('admin')->as('admin.')->group(function () {
	Route::get('/', [AdminController::class, 'index'])->name('index');
	Route::get('/topics', [AdminController::class, 'topics'])->name('topics');
	Route::get('/posts', [AdminController::class, 'posts'])->name('posts');
	Route::get('/calls', [AdminController::class, 'calls'])->name('calls');
	Route::get('/users', [AdminController::class, 'users'])->name('users');

	Route::prefix('posts')->as('posts.')->group(function () {
		Route::get('/create', [AdminController::class, 'createPost'])->name('create');
		Route::get('/{post}', [AdminController::class, 'showPost'])->name('show');
		Route::get('/{post}/edit', [AdminController::class, 'editPost'])->name('edit');
	});

	Route::prefix('topics')->as('topics.')->group(function () {
		Route::get('/create', [AdminController::class, 'createTopic'])->name('create');
		Route::get('/{topic}', [AdminController::class, 'showTopic'])->name('show');
		Route::get('/{topic}/edit', [AdminController::class, 'editTopic'])->name('edit');
	});

	Route::prefix('users')->as('users.')->group(function () {
		Route::get('/{user}', [AdminController::class, 'showUser'])->name('show');
		Route::get('/{user}/edit', [AdminController::class, 'editUser'])->name('edit');
	});
});

Route::middleware('auth', 'verified')->group(function () {
	Route::get('dashboard', [SandboxController::class, 'dashboard'])->name('dashboard');
	Route::get('learn', [SandboxController::class, 'learn'])->name('learn');
	Route::get('capture', [SandboxController::class, 'capture'])->name('capture');
	Route::post('capture', [SandboxController::class, 'storeCapture'])->name('capture.store');
	Route::get('profile', [SandboxController::class, 'profile'])->name('profile');

	Route::prefix('ajax')->group(function () {
		Route::get('user', [AjaxController::class, 'user'])->name('ajax.user');
		Route::get('charts', [AjaxController::class, 'chartsData'])->name('ajax.charts');
	});
});


/*Route::get('auth/{provider}', [LoginController::class, 'redirectToProvidedProvider']);
Route::get('auth/{provider}/callback', [LoginController::class, 'handleProvidedProviderCallback']);*/
Route::get('auth/{provider}/webhook', [LoginController::class, 'handleProvidedProviderWebhook']);
Route::get('auth/{provider}/webhook', [LoginController::class, 'handleProvidedProviderWebhook']);


//Route::get('/user', [UserController::class, 'index']);
/*Route::prefix('auth')->group(function () {
	Route::prefix('zoom')->group(function () {
		Route::get('', [ZoomController::class, 'authorize'])->name('authorize');
		Route::get('callback', [ZoomController::class, 'callback'])->name('callback');
		Route::get('webhooks', [ZoomController::class, 'webhooks'])->name('webhooks');
	});
});*/
