<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\IOController;

/*
|--------------------------------------------------------------------------
| IO API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register IO routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "io" middleware group. Now create something great!
|
*/

Route::as('io.')->group(function () {
	Route::get('/', [IOController::class, 'index'])->name('welcome');

	Route::get('/captures', [IOController::class, 'captures'])->name('captures');

	Route::get('/calls', [IOController::class, 'calls'])->name('calls');
	Route::get('/calls/{call}', [IOController::class, 'callById'])->name('calls.view');

	Route::get('/analytics', [IOController::class, 'analytics'])->name('analytics');
	Route::get('/analytics/{analytics}', [IOController::class, 'analyticsById'])->name('analytics.view');
});
