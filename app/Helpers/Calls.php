<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Call;
use App\Models\CallAnalytics;

class Calls {
	const TOTAL_WORDS_IN_CALL = 1000.00;
	const EMOTIONS = ['angry', 'happy', 'frustrated', 'surprised', 'fearful', 'disgusted'];
	const KEYWORDS = ['implementation', 'team', 'testing'];

	public static function compileForCharts($start, $end) {
		$charts = [];

		$startDate 	= $start->startOfDay();
		$endDate 	= $end->endOfDay();

		$results = Call::with('analytics')
			->where('user_id', auth()->id())
			->whereHas('analytics')
			->whereDate('started_at', '>', $startDate)
			->whereDate('started_at', '<', $endDate)
			->get();

		$calls = collect($results);

		$voiceSeniority = [];
		$voiceSeniority['labels'] 	= $calls->pluck('metadata.title');
		$voiceSeniority['data'] 	= $calls->pluck('analytics.statistics.voice_seniority');

		$voiceDeliberateness = [];
		$voiceDeliberateness['labels'] 	= $calls->pluck('metadata.title');
		$voiceDeliberateness['data'] 	= $calls->pluck('analytics.statistics.voice_deliberateness');


		$fillerWords['labels'] 	= $calls->pluck('metadata.title');
		$fillerWords['data'] 	= $calls->map(function ($call, $i) {
			//dd(collect($call->analytics->filler_words));
			return intval(Calls::TOTAL_WORDS_IN_CALL / floatval(collect($call->analytics->filler_words)->sum()));
		});//sum('analytics.filler_words');


		$fillerWordKeys = collect($calls->map(function ($call, $i) {
			return collect($call->analytics->filler_words)->keys(); 
		}))->flatten(1)
			->unique()
			->map(function($fillerWord, $j) use ($calls) {
				return ['word' => $fillerWord, 'n' => $calls->map(function ($innerCall, $k) use ($fillerWord) {
					return $innerCall->analytics->filler_words[$fillerWord];
				})->sum()];
			})->values()->keyBy('word')->map(function ($item, $l) { 
				return ['x' => $item['word'], 'value' => $item['n']]; 	
			})->values()->all();

		$nFillerWords = ['tags' => $fillerWordKeys];


		$vibes = [];

		$vibes['yours'] = [];
		$vibes['yours']['labels'] 	= self::EMOTIONS;
		$vibes['yours']['data'] 	= collect(self::EMOTIONS)->map(function ($emotion, $i) use ($calls) {
			return $calls->map(function ($innerCall, $j) use ($emotion) {
				return intval($innerCall->analytics['emotions']['yours'][$emotion] * 100.00);
			})->avg();
		});

		$vibes['others'] = [];
		$vibes['others']['labels'] 	= self::EMOTIONS;
		$vibes['others']['data'] 	= collect(self::EMOTIONS)->map(function ($emotion, $i) use ($calls) {
			return $calls->map(function ($innerCall, $j) use ($emotion) {
				return intval($innerCall->analytics['emotions']['others'][$emotion] * 100.00);
			})->avg();
		});

		$emotionIntensityDatasetsX = collect(self::EMOTIONS)
			->map(function ($emotion, $i) use ($calls) {
				$values = $calls->map(function ($innerCall, $j) use ($emotion) {
					return intval($innerCall->analytics['emotions']['yours'][$emotion] * 100.00);
				})->values()->all();

				return $values;//['title' => $emotion, 'values' => $values];
			});

		$emotionIntensityX = ['datasets' => $emotionIntensityDatasetsX];

		$emotionIntensityDatasets = collect(self::EMOTIONS)
			->map(function ($emotion, $i) use ($calls) {
				$values = $calls->map(function ($innerCall, $j) use ($emotion) {
					$x = intval($innerCall->analytics['emotions']['your_peaks'][$emotion]['timestamp']);

					$y = intval($innerCall->analytics['emotions']['your_peaks'][$emotion]['value'] * 100.00);

					return ['x' => $x, 'y' => $y];
				})->values()->all();

				return $values;//['title' => $emotion, 'values' => $values];
			});

		$emotionIntensity = ['data' => [
			'datasets' => $emotionIntensityDatasets,
		]];

		$contributions = ['data' => [
			'yours' => $calls->map(function ($call, $i) {
				return intval(100.00 * floatval($call->analytics->statistics['your_contribution']));
			})->values()->all(),
			'others' => $calls->map(function ($call, $i) {
				return intval(100.00 * (1.00 - floatval($call->analytics->statistics['your_contribution'])));
			})->values()->all(),
		]];

		$contributions['labels'] 	= $calls->pluck('metadata.title');

		$dynamicsContribution = ['data' => $contributions];

		$contentTopicsData = collect(self::KEYWORDS)
			->map(function ($keyword, $i) use ($calls) {
				$keywordValue = $calls->map(function ($innerCall, $j) use ($keyword) {
					return $innerCall->analytics['keywords'][$keyword];
				})->sum();
				return ['x' => $keyword, 'value' => $keywordValue];
			});

		$contentTopics = ['data' => $contentTopicsData];

		$charts['voice-seniority'] = $voiceSeniority;
		$charts['voice-deliberateness'] = $voiceDeliberateness;
		$charts['filler-words'] = $fillerWords;
		$charts['n-filler-words'] = $nFillerWords;
		$charts['vibe-yours'] = $vibes['yours'];
		$charts['vibe-others'] = $vibes['others'];
		$charts['vibe-emotion-intensity'] = $emotionIntensity;
		$charts['dynamics-contribution'] = $dynamicsContribution;
		$charts['content-topics'] = $contentTopics;

		//$charts['vibe-emotion-intensity_X'] = ['data' => $emotionIntensityX];

		return $charts;
	}

	public static function compileForChartsDemo($start, $end) {
		$charts = [];

    	$voiceSeniority = collect([])
    		->add(['label' => 'Call 1 (26-11-89 2:25pm)', 'data' => 45])
    		->add(['label' => 'Call 2 (26-11-89 2:25pm)', 'data' => 61])
    		->add(['label' => 'Call 3 (26-11-89 2:25pm)', 'data' => 37])
    		->add(['label' => 'Call 4 (26-11-89 2:25pm)', 'data' => 7])
    		->add(['label' => 'Call 5 (26-11-89 2:25pm)', 'data' => 97]);

    	$voiceDeliberateness = collect([])
    		->add(['label' => 'Call 1 (26-11-89 2:25pm)', 'data' => 22])
    		->add(['label' => 'Call 2 (26-11-89 2:25pm)', 'data' => 31])
    		->add(['label' => 'Call 3 (26-11-89 2:25pm)', 'data' => 77])
    		->add(['label' => 'Call 4 (26-11-89 2:25pm)', 'data' => 78])
    		->add(['label' => 'Call 5 (26-11-89 2:25pm)', 'data' => 11]);

    	$fillerWords = collect([])
    		->add(['label' => 'Call 1 (26-11-89 2:25pm)', 'data' => 5])
    		->add(['label' => 'Call 2 (26-11-89 2:25pm)', 'data' => 8])
    		->add(['label' => 'Call 3 (26-11-89 2:25pm)', 'data' => 6])
    		->add(['label' => 'Call 4 (26-11-89 2:25pm)', 'data' => 2])
    		->add(['label' => 'Call 5 (26-11-89 2:25pm)', 'data' => 11]);

    	$nFillerWords = collect([])
    		->add(['label' => 'Fuck', 'data' => 5])
    		->add(['label' => 'You', 'data' => 8])
    		->add(['label' => 'A lot', 'data' => 3])
    		->add(['label' => 'Mister', 'data' => 1]);

    	$charts['voice-seniority'] = [
    		'labels' 	=> $voiceSeniority->pluck('label')->all(),
    		'data' 		=> $voiceSeniority->pluck('data')->all()
    	];

    	$charts['voice-deliberateness'] = [
    		'labels' 	=> $voiceDeliberateness->pluck('label')->all(),
    		'data' 		=> $voiceDeliberateness->pluck('data')->all()
    	];

    	$charts['filler-words'] = [
    		'labels' 	=> $fillerWords->pluck('label')->all(),
    		'data' 		=> $fillerWords->pluck('data')->all()
    	];

    	$charts['n-filler-words'] = ['tags' => $nFillerWords->map(function ($item, $i) {
    		return ['x' => $item['label'], 'value' => $item['data']];
    	})
    	->values()
    	->all()];

    	return $charts;
	}

	public static function getTimestampFromRequestOr(Request $request, $key, $returnIfNotFound) {
        $value = $request->has($key) ? trim($request->get($key)) : null;

        if (!isset($value)) {
            return $returnIfNotFound;
        }

        $sanitized = trim(str_replace("&quot;", "", $value));
        $sanitized = trim(str_replace('"', "", $sanitized));

        return Carbon::parse($sanitized);//->setTimezone(auth()->user()->timezone);
    }
}