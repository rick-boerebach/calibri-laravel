<?php
namespace App\Helpers;

use Laravel\Socialite\Facades\Socialite;

class Logins {
	const PARSER_OAUTH_ONE = 'oauth_one';
	const PARSER_OAUTH_TWO = 'oauth_two';
	const PARSER_OAUTH_ALL = 'oauth_all';

	public static function process($provider, $parser = self::PARSER_OAUTH_TWO) {
		$user = Socialite::driver($provider)->user();

		$remoteProfile = self::parseRemoteProfile($user, $parser);

		dd([$provider => $remoteProfile]);

		// persist remote profile and link to user

		return $remoteProfile;
	}

	public static function parseRemoteProfile($user, $parser = self::PARSER_OAUTH_TWO) {
		// OAuth Two Providers
		/*$token = $user->token;
		$refreshToken = $user->refreshToken; // not always provided
		$expiresIn = $user->expiresIn;*/

		// OAuth One Providers
		/*$token = $user->token;
		$tokenSecret = $user->tokenSecret;*/

		// All Providers
		/*$user->getId();
		$user->getNickname();
		$user->getName();
		$user->getEmail();
		$user->getAvatar();*/

		$profile = [
			'id' => $user->getId(),
			'nickname' => $user->getNickname(),
			'name' => $user->getName(),
			'email' => $user->getEmail(),
			'avatar' => $user->getAvatar()
		];

		switch($parser) {
			case self::PARSER_OAUTH_TWO:
				$profile = array_merge($profile, [
					// OAuth Two Providers
					'token' => $user->token,
					'refreshToken' => $user->refreshToken, // not always provided
					'expiresIn' => $user->expiresIn
				]);
				break;
			case self::PARSER_OAUTH_ONE:
				$profile = array_merge($profile, [
					// OAuth One Providers
					'token' => $user->token,
					'tokenSecret' => $user->tokenSecret
				]);
				break;
		}

		$externalProfile = ExternalProfile::create($profile);

		return $profile;
	}

	public static function remoteUser($provider, $token) {
		return Socialite::driver($provider)->userFromToken($token);
	}
}