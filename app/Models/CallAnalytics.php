<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CallAnalytics extends Model
{
    use HasFactory, SoftDeletes;

    public $timestamps = true;

    protected $fillable = [
    	'call_id',
    	'statistics',
    	'emotions',
    	'filler_words',
    	'keywords',
    ];

    //statistics.'voice_seniority', // 0 -> 100
    //statistics.'voice_deliberateness', // 0 -> 100
    //statistics.'your_contribution', // 0.0 -> 1.0 | others_contribution = 1.0 - your_contribution
    	

    protected $casts = [
    	'statistics' 	=> 'array',
    	'emotions' 		=> 'array',
    	'filler_words' 	=> 'array',
    	'keywords' 		=> 'array',
    ];

    public function call() {
        return $this->belongsTo(Call::class);
    }
}
