<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use HasFactory, SoftDeletes;

    public $timestamps = true;

    protected $fillable = [
    	'user_id',
    	'topic_id',
    	'title',
    	'content',
    	'avatar',
    	'header',
    	'status',
    	'metadata',
    ];

    protected $casts = [
    	'metadata' => 'array',
        'avatar'    => 'array',
        'header'    => 'array',
    ];

    public function getAvatarUrlAttribute()
    {
        if ($this->avatar != null && false) :
            return asset($this->avatar);
        else :
            return 'https://ui-avatars.com/api/?name=' . str_replace(' ', '+', $this->title) . '&background=fff&color=6777ef&size=100';
        endif;
    }

    public function getHeaderUrlAttribute()
    {
        if ($this->header != null && false) :
            return asset($this->header);
        else :
            return 'https://ui-avatars.com/api/?name=' . str_replace(' ', '+', $this->title) . '&background=fff&color=6777ef&size=500';
        endif;
    }

    public function topic() {
        return $this->belongsTo(Topic::class);
    }
}
