<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Call extends Model
{
    use HasFactory, SoftDeletes;

    public $timestamps = true;

    protected $fillable = [
    	'user_id',
    	'metadata',
    	'started_at',
    	'ended_at',
    ];

    protected $dates = [
    	'started_at',
    	'ended_at',
    ];

    protected $casts = [
    	'metadata' => 'array'
    ];

    public function analytics() {
        return $this->hasOne(CallAnalytics::class);
    }
}
