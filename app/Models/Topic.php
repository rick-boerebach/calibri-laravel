<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Topic extends Model
{
    use HasFactory, SoftDeletes;

    public $timestamps = true;

    protected $fillable = [
    	'user_id',
    	'name',
    	'description',
    	'avatar',
    	'header',
    	'status',
    	'metadata',
    ];

    protected $casts = [
    	'metadata' => 'array',
    	'avatar' 	=> 'array',
    	'header' 	=> 'array',
    ];

    public function getAvatarUrlAttribute()
    {
        if ($this->avatar != null && false) :
            return asset($this->avatar);
        else :
            return 'https://ui-avatars.com/api/?name=' . str_replace(' ', '+', $this->name) . '&background=fff&color=6777ef&size=100';
        endif;
    }

    public function getHeaderUrlAttribute()
    {
        if ($this->header != null && false) :
            return asset($this->header);
        else :
            return 'https://ui-avatars.com/api/?name=' . str_replace(' ', '+', $this->name) . '&background=fff&color=6777ef&size=500';
        endif;
    }

    public function posts() {
        return $this->hasMany(Post::class);
    }
}
