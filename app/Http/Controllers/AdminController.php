<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Topic;
use App\Models\Post;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index');
    }

    public function topics()
    {
        return view('admin.topics');
    }

    public function createTopic()
    {
        return view('admin.topics.create');
    }

    public function editTopic(Topic $topic)
    {
        return view('admin.topics.edit')
            ->withModel($topic);
    }

    public function posts()
    {
        return view('admin.posts');
    }

    public function createPost()
    {
        return view('admin.posts.create');
    }

    public function editPost(Post $post)
    {
        return view('admin.posts.edit')
            ->withModel($post);
    }

    public function calls()
    {
        return view('admin.calls');
    }

    public function users()
    {
        return view('admin.users');
    }
}