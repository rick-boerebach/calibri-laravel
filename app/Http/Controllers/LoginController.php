<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Helpers\Logins;

class LoginController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->check()) {
            return redirect(route('dashboard'));
        }

        return redirect(route('login'))
            ->withStatus('Please log-in to continue.');
    }

    public function redirectToProvidedProvider(Request $request, $provider) {
    	$driver = Socialite::driver($provider);

    	// $driver->stateless()
    	// $driver->setScopes([])

    	if ($provider == 'google') {
    		$driver = $driver->with(['access_type' => 'offline'])->setScopes(['profile', 'email', 'https://www.googleapis.com/auth/drive', 'https://www.googleapis.com/auth/spreadsheets.readonly', 'https://www.googleapis.com/auth/calendar', 'https://www.googleapis.com/auth/calendar.events', 'https://www.googleapis.com/auth/calendar.events.readonly', 'https://www.googleapis.com/auth/calendar.readonly','https://www.googleapis.com/auth/calendar.settings.readonly']);
    	}

    	return $driver->redirect();
    }

    public function handleProvidedProviderCallback(Request $request, $provider) {

    	Logins::process($provider);
    }

    public function handleProvidedProviderWebhook(Request $request, $provider) {
    	$provider = $request->get('provider');

    	\Log::info('Webhook', $request->all());
    }
}
