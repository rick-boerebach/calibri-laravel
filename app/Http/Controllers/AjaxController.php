<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Call;
use App\Models\CallAnalytics;
use Carbon\Carbon;
use App\Helpers\Calls;

class AjaxController extends Controller
{
    public function user(Request $request) {
    	return $request->user();
    }

    public function chartsData(Request $request) {
        $start  = Calls::getTimestampFromRequestOr($request, 'start', Carbon::now()->subDays(29));
        $end    = Calls::getTimestampFromRequestOr($request, 'end', Carbon::now());

        return Calls::compileForCharts($start, $end);
    }

    public function charts(Request $request) {
        $start  = Calls::getTimestampFromRequestOr($request, 'start', Carbon::now()->subDays(29));
        $end    = Calls::getTimestampFromRequestOr($request, 'end', Carbon::now());

    	$data = [];

    	$calls = Call::where('user_id', auth()->id());

    	return $data;
    }
}
