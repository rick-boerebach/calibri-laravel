<?php

namespace App\Http\Controllers;

use App\Models\CallAnalytics;
use Illuminate\Http\Request;

class CallAnalyticsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CallAnalytics  $callAnalytics
     * @return \Illuminate\Http\Response
     */
    public function show(CallAnalytics $callAnalytics)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CallAnalytics  $callAnalytics
     * @return \Illuminate\Http\Response
     */
    public function edit(CallAnalytics $callAnalytics)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CallAnalytics  $callAnalytics
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CallAnalytics $callAnalytics)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CallAnalytics  $callAnalytics
     * @return \Illuminate\Http\Response
     */
    public function destroy(CallAnalytics $callAnalytics)
    {
        //
    }
}
