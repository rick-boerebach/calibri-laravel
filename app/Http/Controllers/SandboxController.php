<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;
use Carbon\Carbon;
use App\Models\Call;
use App\Models\CallAnalytics;
use App\Models\Topic;
use Faker\Generator as FakerGenerator;
use Faker\Factory as FakerFactory;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class SandboxController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sandbox.index');
    }

    public function profile()
    {
        return view('profile');
    }

    public function permissions() {
        //$adminRole = Role::where('name', 'admin')->first();
        //$adminRole->givePermissionTo('manage topics');
        //return response()->json($adminRole);
        return response()->json(auth()->user()->permissionNames);
    }

    public function giveMeAdmin() {
        auth()->user()->assignRole('admin');

        return $this->permissions();
    }

    public function learn() {
        $topics = Topic::with('posts')->get()->all();

        return view('learn')
            ->withTopics($topics);
    }

    public function learnDemo()
    {
        $behaviors = [];

        $winningBehavior = [
            'id' => sizeof($behaviors),
            'title' => 'Winning',
            'posts' => []
        ];

        $winningBehavior['posts'][] = [
            'id' => 123,
            'img' => 'stisla/img/unsplash/eberhard-grossgasteiger-1207565-unsplash.jpg',
            'title' => 'Excepteur sint occaecat cupidatat non proident',
            'content' => 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
        ];

        $winningBehavior['posts'][] = [
            'id' => 124,
            'img' => 'stisla/img/news/img15.jpg',
            'title' => 'Excepteur sint occaecat cupidatat non proident',
            'content' => 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
            'badge' => [
                'type' => 'danger',
                'icon' => 'fire',
                'text' => 'Fucking Awesome',
            ],
        ];

        $winningBehavior['posts'][] = [
            'id' => 125,
            'img' => 'stisla/img/news/img07.jpg',
            'title' => 'Excepteur sint occaecat cupidatat non proident',
            'content' => 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
        ];

        $winningBehavior['posts'][] = [
            'id' => 126,
            'img' => 'stisla/img/news/img02.jpg',
            'title' => 'Excepteur sint occaecat cupidatat non proident',
            'content' => 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
        ];

        $behaviors[] = $winningBehavior;
        $behaviors[] = $winningBehavior;

        return view('learn')
            ->withBehaviors($behaviors);
    }

    public function dashboard()
    {
        $fillerWords = [];
        $fillerWords[] = ['word' => 'Word 1', 'occurences' => 33, 'first' => true];
        $fillerWords[] = ['word' => 'Word 2', 'occurences' => 28];
        $fillerWords[] = ['word' => 'Word 3', 'occurences' => 21];
        $fillerWords[] = ['word' => 'Word 4', 'occurences' => 13];

        return view('dashboard')
            ->withFillerWords($fillerWords);
    }

    public function welcome()
    {
        return view('welcome');
    }

    public function capture()
    {
        return view('capture');
    }

    public function storeCapture(Request $request) {
        $file = tap($request->file('video'))->store('videos');
        $filename = pathinfo($file->hashName(), PATHINFO_FILENAME);

        FFMpeg::fromDisk('local')
            ->open('videos/'.$file->hashName())
            ->export()
            ->toDisk('local')
            ->inFormat(new \FFMpeg\Format\Video\X264('libmp3lame', 'libx264'))
            ->save('converted_videos/'.$filename.'.mp4');

        return response()->json(['status' => 'success']);
    }

    public function seedACall(Request $request) {
            $call = new Call();

            $call->user_id      = auth()->id();
            $call->started_at   = Carbon::now()->subDays(rand(1, 50));
            $call->ended_at     = $call->started_at->addMinutes(25);
            
            $call->metadata = [
                'title'     => 'Follow-up with Hendrick',
                'source'    => 'Zoom',
            ];

            if ($call->save()) {
                $analytics = new CallAnalytics();
                $analytics->call_id = $call->id;
                
                $analytics->statistics = [
                    'voice_seniority'       => 85,
                    'voice_deliberateness'  => 56,
                    'your_contribution'     => 0.65,
                ];

                $analytics->emotions = [
                    'yours'         => [
                        'angry'         => 0.12,
                        'happy'         => 0.06,
                        'frustrated'    => 0.01,
                        'surprised'     => 0.03,
                        'fearful'       => 0.04,
                        'disgusted'     => 0.74,
                    ],
                    'others'        => [
                        'angry'         => 0.06,
                        'happy'         => 0.12,
                        'frustrated'    => 0.74,
                        'surprised'     => 0.04,
                        'fearful'       => 0.03,
                        'disgusted'     => 0.01,
                    ],
                    'your_peaks'    => [
                        'angry'         => ['timestamp' => 600,     'value' => 0.80],
                        'happy'         => ['timestamp' => 30,      'value' => 0.70],
                        'frustrated'    => ['timestamp' => 630,     'value' => 0.90],
                        'surprised'     => ['timestamp' => 1200,    'value' => 0.80],
                        'fearful'       => ['timestamp' => 605,     'value' => 0.99],
                        'disgusted'     => ['timestamp' => 800,     'value' => 0.55],
                    ],
                ];

                $analytics->filler_words = [
                    'umm'       => 14,
                    'actually'  => 2,
                    'i know'    => 4,
                ];

                $analytics->keywords = [
                    'implementation'    => 11,
                    'team'              => 150,
                    'testing'           => 2,
                ];

                if ($analytics->save()) {
                    return response()->json(['call' => $call, 'analytics' => $analytics]);
                }


            }
    } 

    public function seedACallWithRandomness(Request $request) {
        $faker = FakerFactory::create();

        $nMinutes = $faker->numberBetween(10, 165);

        $call = new Call();

        $call->user_id      = auth()->id();
        $call->started_at   = Carbon::now()->subDays(rand(1, 50));
        $call->ended_at     = $call->started_at->addMinutes($nMinutes);
        
        $call->metadata = [
            'title'     => 'Follow-up with '.$faker->name,
            'source'    => 'Zoom',
        ];

        if ($call->save()) {
            $analytics = new CallAnalytics();
            $analytics->call_id = $call->id;
            
            $analytics->statistics = [
                'voice_seniority'       => $faker->numberBetween(0, 100),
                'voice_deliberateness'  => $faker->numberBetween(0, 100),
                'your_contribution'     => $faker->randomFloat(2, 0.00, 1.00),
            ];

            $analytics->emotions = [
                'yours'         => [
                    'angry'         => 0.12,
                    'happy'         => 0.06,
                    'frustrated'    => 0.01,
                    'surprised'     => 0.03,
                    'fearful'       => 0.04,
                    'disgusted'     => 0.74,
                ],
                'others'        => [
                    'angry'         => 0.06,
                    'happy'         => 0.12,
                    'frustrated'    => 0.74,
                    'surprised'     => 0.04,
                    'fearful'       => 0.03,
                    'disgusted'     => 0.01,
                ],
                'your_peaks'    => [
                    'angry'         => ['timestamp' => $faker->numberBetween(0, $nMinutes*60), 'value' => $faker->randomFloat(2, 0.00, 1.00)],
                    'happy'         => ['timestamp' => $faker->numberBetween(0, $nMinutes*60),      'value' => $faker->randomFloat(2, 0.00, 1.00)],
                    'frustrated'    => ['timestamp' => $faker->numberBetween(0, $nMinutes*60),     'value' => $faker->randomFloat(2, 0.00, 1.00)],
                    'surprised'     => ['timestamp' => $faker->numberBetween(0, $nMinutes*60),    'value' => $faker->randomFloat(2, 0.00, 1.00)],
                    'fearful'       => ['timestamp' => $faker->numberBetween(0, $nMinutes*60),     'value' => $faker->randomFloat(2, 0.00, 1.00)],
                    'disgusted'     => ['timestamp' => $faker->numberBetween(0, $nMinutes*60),     'value' => $faker->randomFloat(2, 0.00, 1.00)],
                ],
            ];

            $analytics->filler_words = [
                'umm'       => $faker->numberBetween(1, 30),
                'actually'  => $faker->numberBetween(1, 30),
                'i know'    => $faker->numberBetween(1, 30),
            ];

            $analytics->keywords = [
                'implementation'    => $faker->numberBetween(1, 30),
                'team'              => $faker->numberBetween(1, 30),
                'testing'           => $faker->numberBetween(1, 30),
            ];

            if ($analytics->save()) {
                return response()->json(['call' => $call, 'analytics' => $analytics]);
            }


        }
    } 
}