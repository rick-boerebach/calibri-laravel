<?php

namespace App\Http\Controllers;

use App\Domains\Calibri\Models\ExternalProfile;
use Illuminate\Http\Request;

class ExternalProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Domains\Calibri\Models\ExternalProfile  $externalProfile
     * @return \Illuminate\Http\Response
     */
    public function show(ExternalProfile $externalProfile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Domains\Calibri\Models\ExternalProfile  $externalProfile
     * @return \Illuminate\Http\Response
     */
    public function edit(ExternalProfile $externalProfile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Domains\Calibri\Models\ExternalProfile  $externalProfile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExternalProfile $externalProfile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Domains\Calibri\Models\ExternalProfile  $externalProfile
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExternalProfile $externalProfile)
    {
        //
    }
}
