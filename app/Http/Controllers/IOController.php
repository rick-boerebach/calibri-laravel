<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\Call;
use App\Models\CallAnalytics;

class IOController extends Controller
{

	public function index(Request $request) {
		return view('io');
	}

	public function calls(Request $request) {
		return response()->json(Call::get());
	}

	public function callById(Request $request, Call $call) {
		return response()->json($call);
	}

	public function analytics(Request $request) {
		return response()->json(CallAnalytics::get());
	}

	public function analyticsById(Request $request, CallAnalytics $callAnalytics) {
		return response()->json($call);
	}

	public function captures(Request $request) {
		return response()->json([]);
	}

	public function storeAnalytics(Request $request) {
		return response()->json($request->all());
	}
}