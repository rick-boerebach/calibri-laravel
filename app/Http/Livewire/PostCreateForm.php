<?php

namespace App\Http\Livewire;

use App\Models\Post;
use App\Models\Topic;
use Kdion4891\LaravelLivewireForms\ArrayField;
use Kdion4891\LaravelLivewireForms\Field;
use Kdion4891\LaravelLivewireForms\FormComponent;
use Livewire\WithFileUploads;

class PostCreateForm extends FormComponent
{
    use WithFileUploads;

    const OPTIONS_STATUS = ['published', 'draft'];

    public function fields()
    {
        $topicOptions = Topic::orderBy('name')->get()->pluck('id', 'name')->all();

        return [
            Field::make('Title')->input()->rules('required'),
            Field::make('Topic', 'topic_id')->select($topicOptions)->help('Please select a topic'),
            Field::make('Content')->textarea()->default(''),
            Field::make('Status')->select(self::OPTIONS_STATUS)->default('published'),
            Field::make('Avatar')->input('file')->rules('required|image'),
            Field::make('Header')->input('file')->rules('required|image'),
        ];
    }

    public function success()
    {
        $this->form_data['user_id'] = auth()->id();
        $this->form_data['metadata'] = [];

        $avatarFile = request()->file('avatar');
        $headerFile = request()->file('header');

        dd(['avatar' => $avatarFile, 'header' => $headerFile]);

        //dd($this->form_data);

        Post::create($this->form_data);
    }

    public function saveAndStayResponse()
    {
        return redirect()->route('admin.posts.create');
    }

    public function saveAndGoBackResponse()
    {
        return redirect()->route('admin.posts');
    }
}
