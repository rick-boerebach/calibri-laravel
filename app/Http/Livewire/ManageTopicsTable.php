<?php

namespace App\Http\Livewire;

use App\Models\Topic;
use Illuminate\Support\Str;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Livewire\Component;

class ManageTopicsTable extends LivewireDatatable
{
	public $model = Topic::class;

    public function columns()
    {
    	return [
    		Column::name('id')->label('Topic ID')->linkTo('learn'),
    		Column::name('name')->label('Name')->editable(),
    		Column::name('status')->label('Status')->editable(),
    		Column::callback(['id', 'name'], function ($id, $name) {
    			return view('partials.admin.table-actions', ['id' => $id, 'name' => $name, 'type' => 'topics']);
            }),
    	];
    }
}
