<?php

namespace App\Http\Livewire;

use App\Models\Post;
use Illuminate\Support\Str;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Livewire\Component;

class ManagePostsTable extends LivewireDatatable
{
	public $model = Post::class;

    public function columns()
    {
    	return [
    		Column::name('id')->label('Post ID'),
    		Column::name('topic_id')->label('Topic ID'),
    		Column::name('title')->label('Title')->editable(),
    		Column::name('status')->label('Status'),
    	];
    }
}
