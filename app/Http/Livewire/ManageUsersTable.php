<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Str;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\NumberColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Livewire\Component;

class ManageUsersTable extends LivewireDatatable
{
	public $model = User::class;

    public function columns()
    {
    	return [
    		//NumberColumn::name('id')->label('User ID'),
    		DateColumn::name('created_at')->label('Created At'),
    		DateColumn::name('updated_at')->label('Last Modified'),
    		Column::name('name')->label('Name')->editable(),
    		Column::name('email')->label('Email'),
    		Column::callback(['id', 'name'], function ($id, $name) {
    			return view('partials.admin.table-actions', ['id' => $id, 'name' => $name, 'type' => 'users']);
            }),
    	];
    }

    public function editById($id) {

    }
}
