<?php

namespace App\Http\Livewire;

use App\Models\Topic;
use Kdion4891\LaravelLivewireForms\ArrayField;
use Kdion4891\LaravelLivewireForms\Field;
use Kdion4891\LaravelLivewireForms\FormComponent;

class TopicCreateForm extends FormComponent
{
    const OPTIONS_STATUS = ['published', 'draft'];

    public function fields()
    {
        return [
            Field::make('Name')->input()->rules('required'),
            Field::make('Description')->textarea()->default(''),
            Field::make('Status')->select(self::OPTIONS_STATUS)->default('published'),
            Field::make('Image', 'avatar')->file(),
            Field::make('Header Image', 'header')->file(),
        ];
    }

    public function success()
    {
        $this->form_data['user_id'] = auth()->id();
        $this->form_data['metadata'] = [];

        //dd($this->form_data);

        Topic::create($this->form_data);
    }

    public function saveAndStayResponse()
    {
        return redirect()->route('admin.topics.create');
    }

    public function saveAndGoBackResponse()
    {
        return redirect()->route('admin.topics');
    }
}
