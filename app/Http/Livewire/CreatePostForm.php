<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;

class CreatePostForm extends Component
{
	use WithFileUploads;

	public $avatar;
	public $header;

    public function render()
    {
        return view('livewire.create-post-form');
    }

    public function save()
    {
    	$this->validate([
    		'avatar' => 'image'
    	]);

    	$this->avatar->store('user_uploads', 'public');
    }
}
