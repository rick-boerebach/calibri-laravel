<?php

namespace App\Http\Livewire;

use App\Post;
use Kdion4891\LaravelLivewireForms\ArrayField;
use Kdion4891\LaravelLivewireForms\Field;
use Kdion4891\LaravelLivewireForms\FormComponent;

class PostEditForm extends FormComponent
{
    public function fields()
    {
        return [
            Field::make('Name')->input()->rules('required'),
        ];
    }

    public function success()
    {
        Post::create($this->form_data);
    }

    public function saveAndStayResponse()
    {
        return redirect()->route('posts.create');
    }

    public function saveAndGoBackResponse()
    {
        return redirect()->route('posts.index');
    }
}
