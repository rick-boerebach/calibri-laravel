<?php

namespace App\Http\Livewire;

use App\Models\Call;
use Illuminate\Support\Str;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Livewire\Component;

class ManageCallsTable extends LivewireDatatable
{
	public $model = Call::class;

    public function columns()
    {
    	return [
    		Column::name('id')->label('Call ID'),
    		Column::name('user_id')->label('User ID'),
    	];
    }
}
