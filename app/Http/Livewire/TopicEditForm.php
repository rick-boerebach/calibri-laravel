<?php

namespace App\Http\Livewire;

use App\Models\Topic;
use Kdion4891\LaravelLivewireForms\ArrayField;
use Kdion4891\LaravelLivewireForms\Field;
use Kdion4891\LaravelLivewireForms\FormComponent;

class TopicEditForm extends FormComponent
{
    public function fields()
    {
        return [
            Field::make('Name')->input()->rules('required'),
        ];
    }

    public function success()
    {
        $this->model->update($this->form_data);
    }

    public function saveAndStayResponse()
    {
        return redirect()->route('admin.topics.edit');
    }

    public function saveAndGoBackResponse()
    {
        return redirect()->route('admin.topics.index');
    }
}
