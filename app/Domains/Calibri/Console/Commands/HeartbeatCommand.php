<?php

namespace App\Domains\Calibri\Console\Commands;

use Illuminate\Console\Command;

class HeartbeatCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calibri:heartbeat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Flush queues and work away backlog.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \App\Domains\Calibri\Broadcasting\Broadcasting::welcome('Hello from heartbeat');
        return 0;
    }
}
