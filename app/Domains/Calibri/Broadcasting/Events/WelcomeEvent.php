<?php

namespace App\Domains\Calibri\Broadcasting\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Domains\Calibri\Broadcasting\BaseEvent;

class WelcomeEvent extends BaseEvent
{

  public function broadcastOn()
  {
    return ['my-alerts'];
  }

  public function broadcastAs()
  {
      return 'welcome-event';
  }
}