<?php

namespace App\Domains\Calibri\Broadcasting\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Domains\Calibri\Broadcasting\BaseEvent;

class AppointmentEndsEvent extends BaseEvent
{
	public $appointment = null;

	public function __construct($appointment)
	{
		$this->appointment = $appointment;
	}

  public function broadcastOn()
  {
    return ['my-activity'];
  }

  public function broadcastAs()
  {
      return 'appointment.ends';
  }
}