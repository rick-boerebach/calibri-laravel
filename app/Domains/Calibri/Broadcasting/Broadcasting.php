<?php

namespace App\Domains\Calibri\Broadcasting;

use App\Domains\Calibri\Broadcasting\Events\WelcomeEvent;
use App\Domains\Calibri\Broadcasting\Events\MeetingStartsEvent;
use App\Domains\Calibri\Broadcasting\Events\MeetingEndsEvent;

class Broadcasting
{
  public static function welcome($message) {
    event(new WelcomeEvent($message));
  }

  public static function meetingStarts($meeting) {
    event(new MeetingStartsEvent($meeting));
  }

  public static function meetingEnds($meeting) {
    event(new MeetingEndsEvent($meeting));
  }
}